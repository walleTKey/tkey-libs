// Copyright (C) Tillitis AB
// SPDX-License-Identifier: GPL-2.0-only

#ifndef TKEY_BLAKE2S_H
#define TKEY_BLAKE2S_H

#include <stdint.h>

int blake2s(uint8_t out[const], const unsigned long outlen, const uint8_t key[const],
	    const unsigned long keylen, const uint8_t in[const], const unsigned long inlen);

#endif

